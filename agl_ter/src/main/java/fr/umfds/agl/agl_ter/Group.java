package fr.umfds.agl.agl_ter;


import java.util.List;


public class Group {

	private int groupNumber;
	private Subject subject;
	private List<Wish> wishes;
	
	public Group() {
		TERManager.getManager().addGroup(this);
	}
	
	
	public void setSubject(Subject subject) {
		this.subject = subject;
		subject.setGroup(this);
	}
	
	
	public Subject getSubject() {
		return this.subject;
	}
	
	public List<Wish> getWishes() {
		return wishes;
	}

	public void setWishes(List<Wish> wishes) {
		this.wishes = wishes;
	}
	
	
	
	public void assignSubject(Subject subject) {
		this.setSubject(subject);
		subject.setGroup(this);
	}
	
	
	public void addWish(Subject subject, int priority) throws InvalidWishException {
		if(this.wishes.size() < 5) {
			this.wishes.add(new Wish(this, subject, priority));
		}
		else {
			throw new InvalidWishException("Cannot have more than 5 wishes");
		}
	}


	public int getGroupNumber() {
		return groupNumber;
	}


	public void setGroupNumber(int groupNumber) {
		this.groupNumber = groupNumber;
	}
	
	
	
	


	
	
}
