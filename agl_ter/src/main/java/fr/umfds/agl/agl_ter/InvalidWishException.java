package fr.umfds.agl.agl_ter;

class InvalidWishException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidWishException(String string) {
		super(string);
	}

}
