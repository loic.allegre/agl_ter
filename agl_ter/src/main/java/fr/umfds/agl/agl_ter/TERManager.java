package fr.umfds.agl.agl_ter;
import java.util.ArrayList;
import java.util.List;


public class TERManager {

	private static TERManager instance;
	
	private List<Group> groups;
	private List<Subject> subjects;
	private Hongrois hongrois;
	
	
	private TERManager() {
		this.groups = new ArrayList<Group>();
		this.subjects = new ArrayList<Subject>();
	}
	
	
	public static synchronized TERManager getManager() {
		if(instance == null) {
			instance = new TERManager();
		}
		return instance;
	}
	
	public List<Group> getGroups() {
		return groups;
	}
	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}
	public List<Subject> getSubjects() {
		return subjects;
	}
	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}
	
	
	public void addGroup(Group group) {
		this.groups.add(group);
	}
	
	public void removeGroup(Group group) {
		this.groups.remove(group);
	}
	
	public void addSubject(Subject subject) {
		this.subjects.add(subject);
	}
	
	public void removeSubject(Subject subject) {
		this.subjects.remove(subject);
	}
	
	
	public List<List<Integer>> assign(int phase) {
		List<List<Integer>> adjList = new ArrayList<>();
		this.hongrois.setHauteur(this.groups.size());
		this.hongrois.setLargeur(this.subjects.size());
		this.groups.forEach((g) -> {
			g.getWishes().forEach((w) -> {
				List<Integer> wishIntegers = new ArrayList<>();
				wishIntegers.add(g.getGroupNumber());
				wishIntegers.add(w.getSubject().getSubjectNumber());
				wishIntegers.add(w.getPriority());
				adjList.add(wishIntegers);
			});
		});
		this.hongrois.setAdjacenceList(adjList);
		return this.hongrois.affectation(phase);
	}
	
}
