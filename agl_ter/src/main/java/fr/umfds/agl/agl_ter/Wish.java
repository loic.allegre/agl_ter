package fr.umfds.agl.agl_ter;

public class Wish {
	
	private Subject subject;
	private Group group;
	private int priority;
	
	
	public Wish(Group group, Subject subject, int priority) {
		this.group = group;
		this.subject = subject;
		this.priority = priority;
	}
	
	
	public Subject getSubject() {
		return subject;
	}
	
	public void setSubject(Subject subject) {
		this.subject = subject;
	}
	
	public Group getGroup() {
		return group;
	}
	
	public void setGroup(Group group) {
		this.group = group;
	}
	
	public int getPriority() {
		return priority;
	}
	
	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	
	
}
