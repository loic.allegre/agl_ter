package fr.umfds.agl.agl_ter;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;

public class AffectationTest {
	
	Group group;
	Subject subject;
	
	@BeforeEach
	void init() {
		group = new Group();
		subject = new Subject();
	}
	
	
	@Test
	void testAffectation() {
		group.assignSubject(subject);
		assertAll("Test mutual assignment",
				() -> assertEquals(group, subject.getGroup()),
				() -> assertEquals(subject, group.getSubject())
		);
	}
	
}
